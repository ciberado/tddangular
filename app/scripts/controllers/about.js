'use strict';

/**
 * @ngdoc function
 * @name tdddemoApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the tdddemoApp
 */
angular.module('tdddemoApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
