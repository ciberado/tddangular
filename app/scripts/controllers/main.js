'use strict';

/**
 * @ngdoc function
 * @name tdddemoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the tdddemoApp
 */
angular.module('tdddemoApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
